﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
namespace Game
{
public class Level : MonoBehaviour
{
    [Header("Timer")]
    [SerializeField] private bool _timerIsOn;
    [SerializeField] private float _timerValue;
    private Text _timerView;

    [Header("Objects")]
    [SerializeField] private Player _player;
    [SerializeField] private Exit _exitFromLevel;
    [SerializeField] private GameObject _nextLevel;
    private float _timer = 0;
    private bool _gameIsEnded = false;
    private GameObject Canvas;
    private GameObject TextWon;
    private GameObject TextLost;
        private void Awake()
    {
        _timer = _timerValue;
        _timerView = GameObject.Find("TimerText").GetComponent<Text>();

        Canvas = GameObject.Find("Canvas");
        TextWon = Canvas.transform.Find("WinMenu").gameObject;
        TextLost = Canvas.transform.Find("LoseMenu").gameObject ;
    }

    private void Start()
    {
        _exitFromLevel.Close();
    }

    private void Update()
    {
        if(_gameIsEnded)
            return;
        
        TimerTick();
        LookAtPlayerHealth();
        LookAtPlayerInventory();
        TryCompleteLevel();
    }

    private void TimerTick()
    {
        if(_timerIsOn == false || _gameIsEnded)
            return;
        
        _timer -= Time.deltaTime;
        _timerView.text = $"{_timer:F1}";
        
        if(_timer <= 0)
            StartCoroutine(Lose());
    }

    private void TryCompleteLevel()
    {
        if(_exitFromLevel.IsOpen == false)
            return;

        var flatExitPosition = new Vector2(_exitFromLevel.transform.position.x, _exitFromLevel.transform.position.z);
        var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);
        
        if(flatExitPosition == flatPlayerPosition)
            StartCoroutine(Victory());
    }

    private void LookAtPlayerHealth()
    {
        if (_player == null) { return; }
        if(_player.IsAlive)
            return;

        StartCoroutine(Lose());
        Destroy(_player.gameObject);
    }

    private void LookAtPlayerInventory()
    {
        if(_player.HasKey)
            _exitFromLevel.Open();
    }

    IEnumerator Victory()
    {
        _gameIsEnded = true;
        TextWon.SetActive(true);
        TextWon.GetComponent<Text>().text = $"Your Time: {(int)(_timerValue - _timer)}";
        yield return new WaitForSeconds(4);
        TextWon.SetActive(false);
        


        Instantiate(_nextLevel, new Vector3(0, 0, 0), Quaternion.identity);
        Destroy(gameObject);

    }

    IEnumerator Lose()
    {
        _gameIsEnded = true;
        TextLost.SetActive(true);
        TextLost.GetComponent<Text>().text = $"Your Time: {(int)_timerValue - _timer}";
        yield return new WaitForSeconds(4);
        TextLost.SetActive(false);
        
        Debug.LogError("Lose");
    }
}
}