using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game {
public class Bullet : MonoBehaviour
{
    [SerializeField] public Vector3 direction;

    void FixedUpdate()
    {
        transform.Translate(0.5f * direction, Space.World);

    }
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.TryGetComponent(out PointPatroller point_patroller))
        {
             point_patroller.Death();
        }else if (other.TryGetComponent(out PointsObserver point_observer))
        {
            point_observer.Death();

        }
        Destroy(gameObject);
    }
}
}