﻿using UnityEngine;

namespace Game
{
[RequireComponent(typeof(Movement))]
public class Player : MonoBehaviour
    {
    public bool IsAlive { get; private set; } = true;
    public bool HasKey { get; private set; } = false;
    [SerializeField] private GameObject bullet_prefab;
    private Movement _movement;

    private void Awake()
    {
        _movement = GetComponent<Movement>();
    }

    private void Start()
    {
        Enable();
    }

    public void Enable()
    {
        _movement.enabled = true;
    }
    public void Shoot()
    {
        Quaternion rot = Quaternion.Euler(0,90f*_movement.vision_direct.x, 90f * _movement.vision_direct.z);
        GameObject bullet =Instantiate(bullet_prefab, transform.position+ 0.4f* _movement.vision_direct, rot);
        Bullet script = bullet.GetComponent<Bullet>();
        script.direction = _movement.vision_direct;

    }
    public void Disable()
    {
        _movement.enabled = false;
    }

    public void Kill()
    {
        IsAlive = false;
    }

    public void PickUpKey()
    {
        HasKey = true;
    }
}
}