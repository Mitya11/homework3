using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game
{
    public class Teleport : MonoBehaviour
    {
        [SerializeField] private Teleport _terminalEgress;
        [SerializeField] private float tpCooldown;
        [SerializeField] private Player _player;
        private float _timer = 1;
        // Start is called before the first frame update
        void Arrival()
        {
            _timer = tpCooldown;
        }
        void Update()
        {   
            if (_timer >= 0)
            {
                _timer -= Time.deltaTime;
            }
            if (_player == null) { return; }
            var flatTPPosition = new Vector2(gameObject.transform.position.x, gameObject.transform.position.z);
            var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);
            if (flatTPPosition == flatPlayerPosition && _timer < 0)
            {


                _player.transform.position = new Vector3(_terminalEgress.transform.position.x, 
                    _player.transform.position.y, _terminalEgress.transform.position.z);
                _terminalEgress.Arrival();
            }
        }
    }
}