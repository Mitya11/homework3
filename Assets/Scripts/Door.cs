using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game
{
    public class Door : MonoBehaviour
    {
        [SerializeField] private Player _player;
        // Update is called once per frame
        void Update()
        {
            if (_player == null) { return; }
            if (_player.transform.position == gameObject.transform.position)
            {
                Destroy(gameObject);

            }
        }
        public void KeyPickedUp()
        {
            int LayerIgnoreRaycast = LayerMask.NameToLayer("Ignore Raycast");
            gameObject.layer = LayerIgnoreRaycast;
        }
    }
}