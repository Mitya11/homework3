﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
public class Movement : MonoBehaviour
{
    [SerializeField] private LayerMask _obstacleMask;
    [SerializeField] private LayerMask _enemyMask;
    [SerializeField] private float _step;
    public Vector3 vision_direct;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
            TryMove(Vector3.forward);
        
        if(Input.GetKeyDown(KeyCode.DownArrow))
            TryMove(Vector3.back);
        
        if(Input.GetKeyDown(KeyCode.RightArrow))
            TryMove(Vector3.right);
        
        if(Input.GetKeyDown(KeyCode.LeftArrow))
            TryMove(Vector3.left);
        if(Input.GetKeyDown(KeyCode.Space))
            GetComponent<Player>().Shoot();
    }
    
    private void TryMove(Vector3 direction)
    {
        var forwardRay = new Ray(transform.position, direction);
        vision_direct = direction.normalized;
        if (Physics.Raycast(forwardRay, out RaycastHit hit, _step, _obstacleMask | _enemyMask))
            return;

        transform.forward = direction;
        transform.Translate(direction * _step, Space.World);
    }
}
}