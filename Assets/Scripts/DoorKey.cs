using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game { 
public class DoorKey : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Door _door;
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            _door.KeyPickedUp();
            Destroy(gameObject);
        }
    }
}
}